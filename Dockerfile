FROM python:3.6-slim

COPY transformer/requirements.txt /opt/
RUN pip install -r /opt/requirements.txt
RUN pip install gunicorn flask-json

# pre-cache the SequenceTagger models
ARG TRANSFORMER_MODELS
ENV TRANSFORMER_MODELS=${TRANSFORMER_MODELS:-ner}
RUN ["python", "-c", "import re, os\nfrom flair.models import SequenceTagger\nfor model in re.split(r'[\\s,]+', os.environ['TRANSFORMER_MODELS']):\n  SequenceTagger.load(model.split('=',1)[-1])"]

COPY transformer /opt/transformer
COPY app.py run.sh /opt/

WORKDIR /opt

CMD ["./run.sh"]
