#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 10:31:38 2019

@author: didelani
"""
import os
import sys
import numpy as np
from collections import defaultdict, Counter
import re
import json


output_dir = '../data/training/'

def get_tagged_sentence(sent):
    tagged_sentence = ''
    for token, ne in sent:
        tagged_sentence += token
        if ne!='O':
            tagged_sentence += '<'+ne+'>'+ ' ' 
        else:
            tagged_sentence += ' ' 
    return tagged_sentence


def extract_sentences(data_dir):
    data_path = data_dir + 'train.txt'
    with open(data_path) as f:
        docs = f.readlines()

    sentences = []
    n_sent = 0
    sent = []
    list_sent_len = []

    named_entities = defaultdict(lambda: defaultdict(int))
    multi_word_entites = defaultdict(lambda: defaultdict(int))
    sent_per = 0
    print('# tokens', len(docs))
    for i, line in enumerate(docs):
        if len(line) < 3:
            if len(sent) > 0:
                sentences.append(sent)
            sent = []
            list_sent_len.append(sent_per)
            sent_per = 0
            n_sent += 1
        else:
            token_ne = line.strip().split()
            token, ne = token_ne[0], token_ne[-1]
            sent.append((token, ne))
            named_entities[ne][token] += 1

            k = i
            new_token = token + ' '
            while k + 1 < len(docs) and len(docs[k + 1]) > 3 and ne != 'O' and ne[:2] == 'B-':

                n_tok, n_ne = docs[k + 1].strip().split()

                if ne[2:] == n_ne[2:] and n_ne[:2] == 'I-':
                    new_token += n_tok + ' '
                    k += 1
                else:
                    break

            sent_per += 1

            if len(new_token) > len(token) + 1:
                named_entities['MULTI-WORD_' + ne[2:]][new_token] += 1
    print('# of sentences ', len(sentences))

    #for i in range(len(sentences[:10])):
    #    print(get_tagged_sentence(sentences[i]))

    print('# multi-word entity categories', len(multi_word_entites))

    return sentences, list_sent_len, named_entities, multi_word_entites


def write_ne_dict_To_file(text_data, output_dir):
    with open(output_dir, 'w') as fwriter:
        for token_cnt_prob in text_data:
            token, cnt, prob = token_cnt_prob
            fwriter.write(token+'\t'+str(cnt)+ '\t'+str(prob)+'\n')

def write_tsv_file(text_data, output_dir):
    with open(output_dir, 'w') as fwriter:
        for token_tag in text_data:
            token, tag = token_tag
            fwriter.write(token+'\t'+tag+'\n')

def read_conll_data(data_dir_file):
    with open(data_dir_file) as f:
        text_lines = f.readlines()

    tokens_tags = []
    for line in text_lines:
        token_tag = line.strip().split('\t')
        if len(token_tag) > 1:
            token, tag =  token_tag
        else:
            token, tag = '', ''

        tokens_tags.append([token, tag])

    return tokens_tags



def sort_named_entities_by_freq(transf_dir, named_entities):
    
    named_entities_freq = []

    named_entity_to_idx = dict()
    
    k = 0
    for named_entity in named_entities:
        ne_dict = Counter(named_entities[named_entity])
        total_sum = sum(ne_dict.values())
    
        # compute the probability of ne in corpus
        ne_counts = sorted(ne_dict.items(), key=lambda x: x[1], reverse=True)

        nes = []
        for ne, cnt in ne_counts:
            nes.append([ne, cnt, cnt/total_sum])

        named_entities_freq.append(nes)
        named_entity_to_idx[named_entity]=k
        k+=1
        print(named_entity, nes[:5])

        write_ne_dict_To_file(nes, transf_dir+named_entity+'.tsv')

    with open(transf_dir+'named_entity_to_idx.json', 'w') as f:
        json.dump(named_entity_to_idx, f)
        
    return named_entities_freq, named_entity_to_idx




if __name__ == '__main__':
    data_dir = '../data/eu_langs/'

    if len(sys.argv) < 2:
        print("use the default data path: '../data/conll2003/' ")
    else:
        print("Please run the code with: python save_named_entities_ngrams.py [data_path_conll_format] e.g")
        print("python save_named_entities_ngrams.py '../data/training/bio_cased/'  ")
        data_dir = sys.argv[1]

    sentences, list_of_sentence_len, named_entities, multi_word_entites = extract_sentences(data_dir)
    #print(sentences[8816])
    transf_dir = '../data/transformation/'
    if not os.path.exists(transf_dir):
        os.makedirs(transf_dir)

    sorted_ne_freq, ne_to_idx = sort_named_entities_by_freq(transf_dir, named_entities)
