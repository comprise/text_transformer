The text transformer tool helps to:
* identify sensitive words or named entities in a dialog conversation
* Text transformation of sensitive words by words of the same-type or placeholders

This provides the steps you need to take to train your own named entity recognition model that powers the text transformer, we provide a link to a multilingual NER model [here](https://drive.google.com/drive/folders/1swZcPz8wBUgYYThYW0qcbieX4T9pwzPp?usp=sharing) that supports the following languages: 
* German (DE)
* English (EN)
* Spanish (ES)
* French (FR)
* Italian (IT)
* Latvia (LV)
* Dutch (NL)
* Portuguese (PT)

They only support three tags: PER (personal names), ORG (organization), and LOC (location)

----

## Requirements
Python 2.7 or 3 with following packages:
* torch==1.4
* requests==2.25.1
* sentencepiece==0.1.91
* transformers==3.5.1
* flair==0.7

## Usage
### Preprocess named entity recognition dataset. 
First step is to provide an dataset in a CoNLL format i.e (each line has a token and tag separated by a single space while a sentence is separted with an additional line) e.g 

EU B-ORG <br/>
rejects O <br/>
German O <br/>
call O <br/>
to O <br/>
boycott O <br/>
British O <br/>
lamb O <br/>
. O<br/>

Peter B-PER <br/>
Blackburn I-PER <br/>


### Train a named entity recognition model 
We provide a simple code to train a named entity recognition (NER) model using [Flair](https://github.com/zalandoresearch/flair) that is based on BiLSTM-CRF model and word features are obtained from multilingual BERT embeddings. 

> python src/train_ner.py ---input_dir training_data_dir ---output_dir model_output_dir

`training_data_dir`  the directory consisting of the training, development and test data in '.tsv' extension

`model_output_dir`  the directory where the model output, and training log is stored

For example
> python train_ner.py ---input_dir "../data/eu_langs/" ---output_dir "resources/taggers/comprise-ner/


### Add Model to Transformer Directory
Please copy the trained model (***.pt) to ../transformer/io/model/


### Extract named entity ngrams from the training corpus
Extract named entities by running: 
> python save_named_entities_ngrams.py [data_path_conll_format]

Please copy the generated contents from  data/transformation in "model_bulder"  to ../transformer/data/
