import os
import re
import sys
import json
from flask import Flask, request
from flask_json import FlaskJSON, JsonError, json_response
from transformer import transform
import traceback

app = Flask(__name__)
# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)

text_transformer = transform.TextTransformer(models = re.split(r'[\s,]+', os.environ.get('TRANSFORMER_MODELS', 'ner')))

@json_app.invalid_json_error
def invalid_request_error(e):
    """Generates a valid ELG "failure" response if the request cannot be parsed"""
    raise JsonError(status_=400, failure={ 'errors': [
        { 'code':'elg.request.invalid', 'text':'Invalid request message' }
    ] })

def invalid_param_error(name, value):
    raise JsonError(status_=400, failure={ 'errors': [
        { 'code':'elg.param.value.invalid', 'text':'Invalid value {0} for parameter {1}', 'params':[value, name] }
    ] })

@app.route('/')
def home():
    return 'COMPRISE Text Transformer Service'


@app.route('/transform', methods=['POST'])
def transform():
    if request.is_json:
        return transform_elg()
    else:
        return transform_text()

def transform_text():
    params = json.loads(request.args.items().__next__()[0])
    
    text = request.data.decode()
    model = params.get('m', '_default')
    replace_type = params.get('r', 'FULL')
    replace_prob = float(params.get('p', 1.0))
    return '\n'.join(text_transformer.transform(text.splitlines(), replace_type=replace_type, replace_prob=replace_prob, model=model))

def transform_elg():
    data = request.get_json()
    
    # sanity checks on the request message
    if data.get('type') == 'text':
        if 'content' not in data:
            invalid_request_error(None)
        sentences = [x for x in (line.strip() for line in data['content'].splitlines()) if x != '']
    elif data.get('type') == 'structuredText':
        if ('texts' not in data) or any('content' not in text for text in data['texts']):
            invalid_request_error(None)
        sentences = (text['content'] for text in data['texts'])
    else:
        invalid_request_error(None)

    replace_type = "FULL"
    replace_prob = 1.0

    if 'params' in data:
        params = data['params']
        replace_type = params.get('replace_type', 'FULL')
        if replace_type not in ['FULL', 'REDACT', 'WORD']:
            invalid_param_error('replace_type', replace_type)

        replace_prob_str = params.get('replace_prob', '1.0')
        try:
            replace_prob = float(replace_prob_str)
        except:
            invalid_param_error('replace_prob', replace_prob_str)

    try:
        return json_response(response = {
            'type':'texts',
            'texts':[
                {'content':transformed, 'role':'sentence'} for transformed in text_transformer.transform(sentences, replace_type=replace_type, replace_prob=replace_prob)
            ]
        })
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        # Convert any exception into an ELG internal error
        raise JsonError(status_=500, failure={ 'errors': [
            { 'code':'elg.service.internalError', 'text':'Internal error during processing: {0}',
              'params':[traceback.format_exception_only(exc_type, exc_value)[-1]] }
        ]})


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
