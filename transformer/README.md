The text transformer tool helps to:
* identify sensitive words or named entities in a dialog conversation
* Text transformation of sensitive words by words of the same-type or placeholders

----

## Installation and requirements
Python 3 with following packages:
* flair

```
virtualen -p python3 venv
. venv/bin/activate
pip install flair
```

## Usage

```
usage: transform.py [-h] [-l {DEBUG,INFO,WARNING,ERROR,CRITICAL}]
                    [-r {REDACT,WORD,FULL}]
                    [-m path_to_model]
                    input output
```

The default logging is INFO and the default replacement is FULL. Input file is one sentence per line. FULL replaces a multi-word named entity, say "New York" with another multi-word expression e.g San Francisco. WORD replaces the occurence of a single-word entity, say Berlin, with another single-word entity e.g London. REDACT replaces every occurence of a named entity with a placeholder, e.g '▮▮▮▮▮'.

The default model is `ner` (automatically downloaded), but built models can be given in the command line and should be stored in the `io` directory. Other NER models supported (including other languages apart from English) can be found here: [NER models available in FLAIR](https://github.com/flairNLP/flair/blob/master/resources/docs/TUTORIAL_2_TAGGING.md#list-of-pre-trained-sequence-tagger-models). Other European languages with no available models can be trained on publicly available data and stored in the `io` directory. 
