#!/bin/sh

exec gunicorn --bind=0.0.0.0:5000 --timeout 120 "$@" app:app
